import json, os, requests, sys
from time import time
from elasticsearch import Elasticsearch
from datetime import datetime
from dateutil import parser

##
SERVER_URL = os.getenv('ELASTIC_SERVER_URL', default='localhost:9200')
##

if os.getenv('OWASP_ELASTIC_USER') == None :
    print('the user name is empty')
    sys.exit(1) 

if os.getenv('OWASP_ELASTIC_PASS') == None :
    print('the password is empty')
    sys.exit(1) 

#Request to verify that elastic cluster is up
response = requests.get('https://'+os.getenv('OWASP_ELASTIC_USER')+':'+os.getenv('OWASP_ELASTIC_PASS')+'@'+SERVER_URL+'/', verify=False )

if response.status_code == 200 :
    print('elastic is up and running')

#Creating elasticsearch connection
es = Elasticsearch(['https://'+os.getenv('OWASP_ELASTIC_USER')+':'+os.getenv('OWASP_ELASTIC_PASS')+'@localhost:9201/'], verify_certs=False, ssl_show_warn=False)

if os.path.isfile('dast-report.json') :
    with open('dast-report.json') as file:
        report = json.load(file)
        site = report['site']
        date = report['@generated']
        #Second Option
        # timestamp = datetime.strptime(date, '%a, %d %b %Y %H:%M:%S')
        # timestamp = timestamp.isoformat(timespec='milliseconds')
        #Parse the date to ISO Format
        timestamp = parser.parse(date)
        site_dict = site[0]
        site_url = site_dict['@name']
        ssl_enabled = site_dict['@ssl']
        alerts_list = site_dict['alerts']
        alert_number = 1
        for alert in alerts_list:
            alert_plug_id = alert['pluginid']
            alert['site_url'] = site_url
            alert['ssl_enabled'] = ssl_enabled
            alert['timestamp'] = timestamp
            alert['pipeline'] = os.getenv('CI_PIPELINE_ID')
            alert['project'] = os.getenv('CI_PROJECT_NAME')
            alert['job'] = os.getenv('CI_JOB_ID')
            res = es.create(index='owasp-report', id=(os.getenv('CI_PROJECT_NAME')+'-'+os.getenv('CI_PIPELINE_ID')+'-'+os.getenv('CI_JOB_ID')+'-'+str(alert_number)), document=alert)
            alert_number += 1
            print('Alert: '+alert_plug_id+' pipeline: '+os.getenv('CI_PIPELINE_ID')+' job: '+os.getenv('CI_JOB_ID') +' ' +res['result'])
else :
    print('the file dast-report.json doesnt exists')
    sys.exit(1)
