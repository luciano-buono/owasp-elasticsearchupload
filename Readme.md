docker exec -it es01 /usr/share/elasticsearch/bin/elasticsearch-reset-password
docker cp es01:/usr/share/elasticsearch/config/certs/http_ca.crt .
curl --cacert http_ca.crt -u elastic https://localhost:9200

curl -X PUT -u $OWASP_ELACTIC_USER:$OWASP_ELASTIC_PASS https://localhost:9201/owasp-report --insecure

1. WSL2 Ubuntu. Lucho-desktop
curl --cacert http_ca.crt -u elastic:'382*zv1Wz=K9B5qtHAWw' https://localhost:9200

2. Create Index.
curl -X PUT --cacert http_ca.crt -u elastic:'382*zv1Wz=K9B5qtHAWw' https://localhost:9200/owasp-report

3. Create doc
curl -X PUT --cacert http_ca.crt -u elastic:'382*zv1Wz=K9B5qtHAWw' https://localhost:9200/owasp-report/_doc/1  -H 'Content-Type: application/json' -d'{"name": "Mark Heath" }'
3. View document
curl --cacert http_ca.crt -u elastic:'382*zv1Wz=K9B5qtHAWw' https://localhost:9200/owasp-report/_search?pretty

4. env vars
export OWASP_ELASTIC_USER=elastic
export OWASP_ELASTIC_PASS=382*zv1Wz=K9B5qtHAWw
5. env vars pipeline
export CI_PROJECT_NAME=TESTEANDO
export CI_JOB_ID=1
export CI_PIPELINE_ID=1
6. Delete index
curl -X DELETE --cacert http_ca.crt -u elastic:'382*zv1Wz=K9B5qtHAWw' https://localhost:9200/owasp-report/


## Kibana Token
 docker exec -it elasticsearch-elastic-1 /usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s kibana\n
 ## Kibana Code
 docker exec -it kibana bin/kibana-verification-code



 curl -X DELETE --cacert http_ca.crt -u $OWASP_ELASTIC_USER:$OWASP_ELASTIC_PASS https://ELASTIC_SERVER_URL/owasp-report